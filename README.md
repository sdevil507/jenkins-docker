# jenkins集成alpine/oracle-jdk/maven/git的docker镜像

## 说明

项目参照[官方镜像项目](https://github.com/jenkinsci/docker)修改了部分逻辑，具体修改请查看Dockerfile文件中注释!

修改了原先debian+openjdk构建环境为alpine+oracle-jdk-1.8构建环境

项目使用宿主机提供的docker,因此启动容器时使用docker run -u root命令启动

## 项目集成工具版本

jenkins:编译时获取最新稳定版本(可以在[镜像列表](http://mirrors.jenkins.io/war-stable/latest)中查看)。[使用下载工具下载至本地放入文件内进行build,否则网速会很慢]

linux:alpine

jdk:oracle-jdk 1.8

maven:3.6.3

## 使用

可以使用docker进行build操作，自己构建镜像

```bash
docker build -t sdevil507/jenkins:latest .
```
也可以pull传至docker hub的镜像

```bash
docker pull sdevil507/jenkins:latest
```

## 项目环境

**jenkins**

镜像启动后需要挂载该目录,jenkins启动后所有变更将直接在该目录体现

工作目录:/var/jenkins_home

端口:[8080(应用访问),50000(JNLP)]

**maven**

jenkins启动后,系统管理->系统工具设置时,取消自动安装,并设置如下MAVEN_HOME地址

MAVEN_HOME:`/apache-maven-3.6.3`

maven本地仓库地址:`/var/jenkins_home/.m2/repository`

修改settings.xml中央仓库地址为公司私服仓库地址(http://139.196.4.191:8081/repository/maven-public)

修改settings.xml中本地仓库地址为:

`<localRepository>/var/jenkins_home/.m2/repository</localRepository>`

因为涉及到要调用宿主机docker命令,因此容器启动时我们使用docker run -u root,所以本地仓库地址需要手动去设置,否则在root目录下,每次容器重启都会导致本地jar包丢失


**jdk**

jenkins启动后,系统管理->系统工具设置时,取消自动安装,并设置如下JAVA_HOME地址

JAVA_HOME:`/usr/lib/jvm/default-jvm`

**git**

jenkins启动后,系统管理->系统工具设置时,取消自动安装,并设置如下git命令行地址

命令行地址:`/usr/bin/git`

## 项目运行示例

```bash
# 下载镜像
docker pull sdevil507/jenkins:latest

# [重要]设置本地目录权限(本地目录默认拥有者为root,而容器中jenkins user的uid为1000)
sudo chown -R 1000:1000 /home/jenkins/data

# 运行脚本
docker run \
--name jenkins \
# 指定网络
--net xxx-network \ 
--ip xxx.xxx.xxx.xxx \
# 4G运行性能较好
-m 4G \
# [重要:必须使用root用户才可以调用宿主机的docker命令]
-u root \
-p 8080:8080 -p 50000:50000 \
# 数据保存
-v /home/jenkins/data:/var/jenkins_home \
# 调用宿主机docker必须
-v /var/run/docker.sock:/var/run/docker.sock \
# 容器内调用宿主机docker命令
-v $(which docker):/usr/bin/docker \
-d \
sdevil507/jenkins:latest
```